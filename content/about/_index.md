---
author: "Pablo Portugués"
title: "About"
hidden: true
keywords: [
  "pabloportugues",
  "software",
  "cooking",
  "humans",
  "human behavior",
  "beer"
]
---

I like product, software, cooking and humans, among other things and not necessarily related. I currently live and work in Portugal where I try to enjoy our fine cuisine, our tasteful wine, and beer. Lots of it actually, draft and craft. Hum... yes... beer... a nice cold IPA right about now.
Also, I'm a current owner of a guinea pig 🐹 and sometimes I like animals more than humans (humans are usually fine when they're not assholes).

You can find me on some social networks but you can also find me here and perhaps in real life, with beer and great food, where possible.