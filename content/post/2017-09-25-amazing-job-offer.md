---
author: "Pablo Portugués"
date: 2017-09-25
linktitle: “Amazing job offer” [heart not found]
menu:
  main:
    parent: general
next: 
prev: 
title: “Amazing job offer” [heart not found]
description: A view on how we can improve the heartless communication around job offers
weight: 10
keywords: [
  "job offers",
  "communication",
  "job",
  "work",
  "career"
]
tags: [
    "humans"
]
---

{{< figure src="/post-content/2017-09-25/lodge-of-sorceresses.jpg" class="post-image" >}}

Have you ever opened your email or LinkedIn inbox only to find the following message:

>**Greatest career opportunity ever!**
>
>Dear `<insert-name-here>`,
>
>Company `<insert-awesome-company-title-here>` is currently looking for `<insert-job-title-here>`.
>`<insert-awesome-company-title-here>` is a very big and huge company, maybe the biggest there is, probably. It has like a bunch of offices spread across the globe, like virtually everywhere, except Iceland, it's too cold there. Also, it has more employees than a Metallica concert and last year it had supertrillions worth of revenue (yeah, that much). Oh, and it won like 47 Olympic gold medals for it's work. And something you'll not believe, it's certified by Chuck Norris himself.
>
>If you're interested send us that thing that reduces you to a number with some special properties, you know Cornicopius Vitale.
>
>Kindheartedly and the utmost respect for one such as yourself,
>Sergeant Dick
>Bot Person | Cold Hearten Specialist

Then, like me, you've felt like a number. Just another candidate in a sea of people, where only the crap you put on your CV or LinkedIn page matter and defines you as an individual. I mean, I sure as hell would feel motivated to work in a company who treats me like a number on a spreadsheet, who wouldn't right?

**If a personal assistant like Cortana or Siri feels more human than a person, then we as humans have really fucked up!**

I know that at the end of the day works needs to get done, and the company needs to make a profit, so that ideally you and everyone involved can benefit. I understand that. However, most of us work and interact, (almost) every day, with other people. Maybe in the future that won't be the case (I actually doubt that but I'll concede), however this is the reality we currently live in.

So, as humans, who talk and interact with other humans, we should try to make an effort to work on that. I know it isn't always easy, for a whole variety of reasons, like the fact that you hate talking to Carl because he's a dick, or because you have to send out job offers to >9000 job candidates before lunch, but at the end of the day, we end up losing more than we gain.

If I send out the stupid amount of job offers, my success rate of candidates that'll go on the next phase will probably be very low. As a candidate myself, if I'm in an even okay position at a company, I sure as hell will dismiss every job offer that looks as sterile as [watching hours of a paint drying film](https://www.kickstarter.com/projects/charlielyne/make-the-censors-watch-paint-drying). The people that will likely reply are those who are really keen on getting the fuck out of where they're currently at. Moreover, this is first point of contact, so you're killing more viable candidates than bad guys in the Uncharted series (seriously, it's almost the population of a small country! Very *bloody-esq* for an Indiana-Jones-like story).

The best thing about this for me, is that it isn't really that hard to do. Personally I value the type of communication that's most direct, with the information that really matters and is comprehensible by a toddler. Fancy or complicated speech only complicates things and bugs the message. If you can be as straightforward as what I'm writing here, I'd say your comprehension rate will be pretty good.

Also, if I'm writing to Melinda for a Backend Developer offer, it should feel like I'm actually talking to that person. If she took the time to put out information that helped you reach her profile, why not mention it in that message? It sure as hell is direct, and she'll understand right away what clicked for you.

To this day I haven't met a single person to which the following made any difference in choosing a job: "Company Coconuts & Jelly is really really big, has loads of people working there, makes way too much money and has a gazillion certifications.". 99% of the time that shit doesn't matter! That is not what people really care about, as it has very little impact when looking for a job. Of course this information also matters, but it's just way lower on the scale and it usually matters most when you're already there and believe in the future of that company, not before you join.

People care about things such as the work environment, the tools and technologies they can and will use in their work, their colleagues, their team, their responsibility and power to change things, their recognition, the company's transparency, the work schedule flexibility, the workplace flexibility, and so on and so forth (and pay of course). These are the things that impact you daily, these are the things that make your work life enjoyable or just downright painful.

Upon writing a job offer those are the types of things you should focus on. This also means you probably won't be sending >9000 job offers before lunch, but you'll surely get way more replies this way, and more than that, you'll receive replies from people who share these values and you'll surely prefer working with them, I know I will.

Fun fact, the motivation to write this post came from a recent job offer I received in the terms I described. My first reaction was to sarcastically reply to that person for sucking at their job, however, I gave it a second thought and came to the realization that'll I would just be another self-righteous asshole had I done that. I therefore proceed to reply in a somewhat lengthy message describing what I felt about that message, what I value in these types of communication and an example of something that would make difference for me. Unexpectedly enough, the response was far more positive that I'd ever expected. That person thanked me greatly for the honest feedback and even asked my opinion on a couple of other matters. Way different and hellishly positive outcome I'd ever expect. Goes to show that it **actually makes a difference to give a fuck**.

To close of this post, I'll give an example based on the said message I recently sent out:

>Hi `Yennefer`,
>
>My name is `Triss` and I'm with `Lodge of Sorceresses`, who's currently looking to hire a `Sorcerer` for it's new office, in `Montecalvo`.
>
>`Lodge of Sorceresses` is a successful company with given proof that strives to work with latest technologies in the field of `spells, curses and black magic`. We also value the work environment and the work/leisure for each on of our employees as well as the available tools for our work. The new `Montecalvo` team already has `11` people and we think your profile fits the current offering role of `Enchantress`. On your profile you mention experience in `enchanting`, `sorceressing`, `magic crafts` and `bringing a reign of fire down on your enemies`. These are values that `Lodge of Sorceresses` greatly values.
>
>In that sense, we would love to extend this conversation so we can get to know you better and show you how we work.
>
>Thanks,
>`Triss`

---

As Steve Vai so eloquently put it in Devin Townsend's Retinal Circus show, "Life is all about relationships."