---
author: "Pablo Portugués"
date: 2017-07-04
linktitle: From F to F while on fire in 5...
menu:
  main:
    parent: general
next: 
prev: 
title: From F to F while on fire in 5...
description: Normally you don't start a new job de-motivated and bored. How do people get there?
weight: 10
keywords: [
  "motivation",
  "frustration",
  "lack of interest",
  "dismissal",
  "settling",
  "fear",
  "stress",
  "not giving a fuck"
]
tags: [
    "humans",
    "motivation"
]
---

If you've worked at a, say, regular company, you probably encountered people with whom working was hard. Maybe they complained a lot, or were just annoying, but you felt that they didn't do much about it. You ended up getting tired of their crap. What a pain!

Before quickly dismissing people with those behaviors, I think we should first try to understand what led them there. If you think about it, we are curious by default, we are eager to learn and understand why something ticks this way or that way, so how do we get to the point where we just don't care?

From my experience I'd say that's the result of several stages, such as _frustration_, _lack of interest_, _dismissal_, _settling_ and _fear_. I have experienced them all and saw them in other people as well. Perhaps you did to. Here's my intake on their natural progression:

### Frustration

{{< figure src="/post-content/2017-07-04/this-is-the-worst.gif" class="post-image" >}}

As a new employee with fresh ground and two sacks of confidence, you quickly take notice in a lot of stuff that can be bettered, so you take initiative and start working on it. You quickly find out that what you think is broken is never gonna change, either because some people think it is currently working fine and don't understand your point of view (they've grown used to it, so why change?), or because there is a shit-ton of bureaucracy plus costs involved and that's just not worth it. You then proceed to do a jaw drop when you find out it's supposed to be broken!

Unfortunately for me, I actually saw several scenarios where this happened. Something was clearly broken to provide leverage for certain people (this was during my soap opera experience by the way. Strange memories I have...). Very rarely you end up understanding why something that's broken, won't change just because it makes sense not to!

### Lack of interest

{{< figure src="/post-content/2017-07-04/not-my-problem.gif" class="post-image" >}}

Natural follow-up of the previous. You get to a point where you have the will to change but you can do nothing about it, and you just can't seem to find a reason to care, because why would you?

### Dismissal

{{< figure src="/post-content/2017-07-04/fucks-given.gif" class="post-image" >}}

All focus goes on yourself and your own needs, and you start dismissing whatever someone tells you that's important. No matter if they say it's for the common good, or even for your own good, you just do the exact amount of work you have to do and dismiss everything else you can.

Congratulations, you leveled up, you're now on the stage of _not giving a fuck_!

### Settling

{{< figure src="/post-content/2017-07-04/looks-like-im-working.gif" class="post-image" >}}

You conclude that you don't really like where you work or how you do work, but the pay is not that bad, there's also the health insurance and those tasty cookies on Fridays are soooo good!

You got to that sweet spot between not working too hard, but just enough so that your position isn't at risk. Everyday is pretty much the same, regular like-clock routine ensues.

### Fear

{{< figure src="/post-content/2017-07-04/fear.gif" class="post-image" >}}

I saw this in two forms: the first was because the benefits and position were good enough to sustain whatever lifestyle the person had (I wouldn't call having kids a lifestyle, but it applies for the point that I'm making). Thus meaning that a job change had to be very carefully weighed in. (and the cookies, can't forget that!)

The second one manifested itself usually through the form of constant _boss-is-crap_, _computer-is-crap_, _work-is-crap_, _its-all-crap_ very positive and constructive attitude. In my first week at a job, I had a guy constantly bugging me on why I had accepted that offer. He strongly believed I would regret that decision and that I would hate working there. I proceeded to asking him: "So why are you still here?" to which he replied: "That's a great question to which I don't know the answer". That person was later fired (stayed too much time of step _settling_ and was a dick).

#### "Let's go already"

Point here is, there are tons of reasons why people don't want to change, and I only figured that out through my years working among different people with very different motivations. Some you understand, some you don't, but either way, it sucks to work with someone that's not motivated and that has reached any (or all!) the steps I described above. And it can't be easy on themselves, I mean, why would you willingly want to be miserable? (unless you're Russel Crowe in _Les Misérables_. That's just painful!)

Most of us have already been there and we know how it sucks. Not caring ends up hurting everyone and is way worse on the long term. Working with motivated and excited people is a thousand times better than someone who feels like they need to be there.

So what I try to do, and bestow on other people to do as well, is to try and help those who are frustrated and in need of help, even if that person is ourselves! That could mean being part of the team that's trying to change something, helping people find a different job or simply by talking and figuring out why they lost their motivation in the first place and how we could help them get it back.

Exploring different things besides work can be also great motivator. Sometimes you just feel burned out and it's not that the work itself is bad, or that the people you work with are annoying, but because you just need something completely different to stimulate your mind and get you to not think about work all the time!

Go and actually take a hike (yes, actual hiking)! Strap up the bike and ride longer than you thought you possibly could! Learn an instrument, join a band and discover a whole new world (and suck at it, but while having loads of fun)! Hell, even gardening is awesome! A friend of mine took his interest in IoT and spicy food and is now growing backyard ghost peppers on an automated system that requires little care (he's actually the only one that can eat them. Something to do with Jalapeños mixed with the milk bottle).

Go back and figure out what motivated you in the first place, it's a good reference for when you get lost. And even if you end up concluding that's no longer the place for you, that is absolutely fine! I find it very stimulating and an awesome learning experience to work at different places, with different people and different problems. Life is made of experiences, don't tie yourself to something that makes you unhappy, rather try to constantly go the opposite direction. And yes, this does not happen overnight, but it's up to you to make it happen.

---
Remember, we're humans and _it's complicated_.

1. Frustration - [The Office US](http://www.imdb.com/title/tt0386676/)
1. Lack of interest - [The IT Crowd](http://www.imdb.com/title/tt0487831/)
1. Dismissal - [Pocahontas](http://www.imdb.com/title/tt0114148/)
1. Settling - [Office Space](http://www.imdb.com/title/tt0151804/)
1. Fear - [Futurama](http://www.imdb.com/title/tt0149460/)