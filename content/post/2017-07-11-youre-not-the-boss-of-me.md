---
author: "Pablo Portugués"
date: 2017-07-11
linktitle: You're not the boss of me!
menu:
  main:
    parent: general
next: 
prev: 
title: You're not the boss of me!
description: Wether hierarchy is a pyramid or a plain, it shouldn't make all that difference
weight: 10
keywords: [
  "hierarchy",
  "flat hierarchy",
  "management",
  "boss",
  "responsibility",
  "organization"
]
tags: [
    "humans",
    "management",
    "responsibility"
]
---

Up until my current job I've worked in places where hierarchy was very high and very clear (okay, not always clear, but clear in the sense that when in doubt that person is probably your boss or ranking higher than you). For a lot of companies (not just software and not just in Portugal), that is the de-facto on how to run a company. Because of that we have a lot of experience and opinions about a strong hierarchy, and most importantly, what's wrong with it.

For anyone that worked in such an environment, it's easier to remember times where you were told what to do on a regular basis and when you asked thing like: "Doesn't this make more sense?" or "Why is there a giraffe in the office?" you would get answers like: "This is how you have to do it", "Someone wants it that way, so I guess that answers your question", "Because I want you to" or "This is Daphne and she's taking us to higher standards! AH! _higher_ get it? 'cause she's a giraffe..." - not funny Dave!

Working based on someone's whim, having any kind of agenda to leverage oneself towards a promotion, even screwing up a work colleague, were concerns that would haunt my mind! If you somehow relate to this, my condolences. Looking back, what most bothered me at those times was the wasteful amount of time and energy on crap. I felt like I was a living in a soap opera, but without the good parts (yes, there can be some, especially Brazilian and Colombian, they master drama!).

Around a year-and-a-half ago I got a job at a company that does things very differently, or at least tries to. Without getting into much detail, the company runs on a self-organization methodology with a way flatter structure (not completely flat, I'm pretty sure that's just not feasible, from a practical standpoint that is) and way less bureaucracy. At first glance, that might seem like paradise, especially compared to what you're used to, and I must agree, up until a point, as there is no such thing as a silver bullet. In the end you just end up trading problems.

Working in a place with no bosses can be very appealing, and it does indeed have it's benefits, however, having a boss isn't inherently bad. We as humans, driven by personal motivations and interests, are responsible for the common idea of a boss, that is, a dick (not Dick though, Dick's cool because he plays the guitar).

Having a very high or very low hierarchy, a highly bureaucratic or self-organized approach doesn't make a company good or bad, highly or lowly productive, or even a good place to work at! A good workplace needs a must-have ingredient - **Culture** - and with culture comes great responsibility.

### Culture of Collaboration

In Portugal companies tend to use the term _collaborators_ when referring to employees, because the word _employees_ is usually associated with submission to another and doesn't really give a collaborative feel to who you are in a company. The main reason for this is because the word gives the sense that you actually contribute with something, that your voice is heard, that your opinion matters. In most cases that's not true, but in a few the opposite is very true and when that happens, a whole more does as well.

Collaboration happens when people are involved in something, and they have a voice and an opinion on the matter. When an idea is discussed among a group of people that are both affected and capable of figuring out a solution. It's basically people doing their jobs and feeling good doing them (such a cliché I know, but they're not all bad).

The beauty of this is that it can happen in both scenarios of what I mentioned above (hierarchy or no hierarchy). You can have a boss and actually be a collaborator, because when you tell that person: "If you go down this road, we'll all loose. I've tested this with and without the giraffe and got way better results with a llama" that person listens to you, gives you credit and trusts you, because they know you're responsible and serious about your work, and that you want what's best for everyone (which most of the time is what's best for the company, and usually, the opposite is also true).

If you think about it, it's so straightforward and just makes plain sense. **Someone hires you to do a job, and they trust you to do that job. Why do we complicate so much?**

At the end of the day, having a giraffe or a llama (nothing personal Daphne, I have you in very _high_ consideration), bosses or no bosses, you have to work together for the common good. You have to be respectful to the people around you and be responsible for your work. Those are very basic features that help your company, your clients, your work colleagues (that are sometimes your friends) and you! Don't be a dick. Put yourself in the other person's shoes and think for a minute (or two). Take into consideration others knowledge and skill. Take heed to people's advices! Hell, ask advice!

By doing this you're helping create a workplace that you feel comfortable and happy working in, you take pride on what you do, and you actually feel good about it.

---
When in doubt, just apply a dash of common sense, because 99% of problems are solved with just that. (either that or: _What would my dog do?_)

If you want to read my take on the common problems affecting motivation and enthusiasm on people, go [here]({{< ref "2017-07-04-from-f-to-f-on-fire-in-5.md" >}}).