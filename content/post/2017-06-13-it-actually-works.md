---
author: "Pablo Portugués"
date: 2017-06-13
linktitle: It actually works
menu:
  main:
    parent: general
next: 
prev: 
title: It actually works!!
description: Why and how I set this blog up
weight: 10
keywords: [
  "Hugo",
  "static website",
  "GitLab",
  "Open source",
  "GitLab",
  "Jekyll",
  "Staticman",
  "GitHub"
]
tags: [
    "general"
]
---

For some time now I wanted to write something that would, hopefully, help others as much as other blogs across the web have helped me. Looking at the available tools out there, I didn't like the idea of not controlling my data and on some instances, having to pay for it.

### Location, location, location

Loving the open source and the community around it, I eventually found out that GitHub had support for static pages. _My oh my!_ I thought.
Searched how I could have a not so shitty UI to present text to people, and ended up meeting Jekyll and the crazy amount of themes available for free (a bunch of them developed/forked and offered by the community).

Fast-forward and lot of procrastination later, I got acquainted with GitLab. Now, I'm a big fan of companies cultures and GitLab has a [very interesting one](https://about.gitlab.com/culture/). I actually know of a person who works there, so every now and then I get the chance to ask a bunch of questions about how they work (they're remote only btw) and what impact their culture has on their work. After the [recent issue](https://about.gitlab.com/2017/02/01/gitlab-dot-com-database-incident/) they had with their production database, I was baffled by the way they approached it. From the moment the problem occurred, to how the whole company got together, and how they worked to resolve the issue. [Constant communication](https://docs.google.com/document/d/1GCK53YDcBWQveod9kfzW-VCxIABGiryG7_z_6jHdVik/pub) for the whole wide world, even [live meeting casts](https://www.youtube.com/watch?v=nc0hPGerSd4) of the people who were trying to fix the problem! People from outside GitLab were watching this happen and sent suggestions and offered to help!
They did a full [disclosure on the issue](https://about.gitlab.com/2017/02/10/postmortem-of-database-outage-of-january-31/) and embraced it as part of them. They learned and prospered from it, and I strongly believe they're better for it. To that, hats off!

With that major event in mind, and given the fact that GitLab CE is open source ([GitHub isn't if you didn't know](https://about.gitlab.com/2016/07/20/gitlab-is-open-core-github-is-closed-source/)) and that they also offer static page support, the choice was an easy one.

### Hammers and nails

Moving on to the tool that helped make this possible. As I mentioned earlier, I started of with Jekyll. [Setup for a default site](http://jekyllrb.com/docs/quickstart/) is pretty forward as is setting up local development. Having so many themes available, I was looking for something simple and minimalist that would work great for a blog. Downloaded a few of them and setting up local development didn't go as smoothly as I'd expect. Bear in mind that to run Jekyll you need Ruby installed. Problems with gem dependencies were fairly common and I struggled to find compatible versions for the gems that weren't downloading.

All in all, I did manage to set up a site and when I was proudly commenting that with a friend, he said: "take a look at Hugo. It's written in Go and I think you'll like it". As soon as I got the chance, I started messing with Hugo and soon after I had a site up and running. The theme offering isn't as vast as Jekyll's, but when there are vast options, there is much dilemma and indecision, mostly just nit picking.
To my misery I did do some customization to the theme I forked (which itself is a fork of another fork of the original theme). I reminded myself a little bit how much I hate CSS.

All that crap later, removed the bloatware (Disqus support and other JS-I-like-to-make-hundreds-of-requests thinguies), changed the info, added some new icons and ta-da! The yet another blog with not so crappy UI is available to everyone! Yey! :D

### What about them comments?

One of the requisites I had for my blog, was that it had to support comments. I find myself digging through comments on other blog's because there is so much valuable information there (also quite a few unhelpful data as well) and I want to encourage people to discuss and comment their views and thoughts about my rants.
Unfortunately, the only out-of-the-box comment support systems for static sites like Jekyll or Hugo are all closed source with the data living on their servers. Since I'm building a static website mainly because I want to have ownership and control of the data, handing the comments out to some other entity doesn't seem like a smart move.

Enter [Staticman](https://staticman.net/) (fun fact, made by a Portuguese guy)! Basically the guy had the same problem and came up with a fairly simple and reliable solution. You have a webserver running Staticman and your comments form sends a POST request with the comment info. It then triggers a pull request to your own repo with that comment info. You just need to enable your site to support reading comments from a specific folder in your repository, that will hold the comments info, and each time you merge PR's you'll get the site updated. He even setup an instance in his own AWS account for anyone to use, you just need to add `statimanapp` github user to your repo. Sadly though, it just supports GitHub pages, at least for now.
That being said, until I can set it up, comments will have to be manual, so you can contact me through the email you'll find on the footer and I'll add them as I go along.

---
I hope this post was useful or interesting for you, if not, no worries, it's useful for me, so I'll try to keep them coming! :)