---
author: "Pablo Portugués"
date: 2017-07-18
linktitle: Why do I smell burning?
menu:
  main:
    parent: general
next: 
prev: 
title: Why do I smell burning?
description: My personal experience of a burnout and what I learned.
weight: 10
keywords: [
  "burnout",
  "stress",
  "frustration"
]
tags: [
    "humans",
    "motivation"
]
---

{{< figure src="/post-content/2017-07-18/hi-dont-mind-my-fire.gif" class="post-image" >}}

Some three months ago, I felt exhausted, frustrated and with no motivation whatsoever. I was at a stage were I got no pleasure in my work and I forced myself to work remotely whenever possible to avoid interaction with other people, in an attempt to help me focus on getting work done, thus regaining confidence and purpose. To my disappointment, no matter what I did, every passing day I just grew more and more detached until I knew **I had to change**. The major impact my working life was having on my personal one was impossible to neglect, and I just couldn't keep it up any longer...

I felt expendable and an idiot for trying to solve problems affecting me, when there was clearly no will to change. I was pushing against a closing wall. It was only later on I discovered I was having a burnout. I had gone past the point of no return and **I had to do something**.

Around that time I went on a one week vacation after which I went straightaway to a tech conference in London. I purposely kept all the work I could at bay, so I could give myself some time away, to reflect on how I got to that point, why I got there, and what could I do about it.

### So it begins

I was on this project for about a year and a half. It was the only project I'd been since back when I started, and although it had already existed for about the same amount of time before me, it was at that moment the turbo kicked in.

I had spent my last two jobs (about three years total) working with C# (WPF and Windows Phone) and the position I was hired for was a .NET position. On my first week at the job I went straight to the client (in the US) with my other only team mate at the time. On my second day, thousands of kilometers away from home (and my first time in the US) I found out the new work we would be doing (the Backend team was gonna increase very rapidly, after all we were just two) was going to be in Java. I remember asking the question twice to make sure I didn't get it wrong. When I finally realized I wasn't, I think _I pooped a little_.

There I was, fucking far away from home, discovering something crazy and unexpected! I mean, I specifically looked for a .NET job since it was the closest thing I had to what I liked working with! I was actually planning on diving really deep in C#. **Holy shit! What do I do?**

I remember going to sleep that night thinking "Hell, I'm still here for a few more days and when I get back to Portugal I'll think about it. After all I'm here to work and I have to keep my game on. Fact is, I've only worked for two days." Next day, something similarly unexpected happened.

Back then two of the co-founders of the company that hired me were there, so the four of us got together to talk, very casually, over a cigarette. They start by asking me "So, you just found out the whole Java thing. Have you worked with Java recently?" to which I replied "Not since college! And that was like 4/5 years ago!". They then explained why was Java chosen and not .NET in a way that made perfect sense. They continued "We know it was unexpected for you, but how do you feel about it? Are you up for it?" to which I honestly answered "Well, you know my Java experience, so do you still think I'm a good fit for the job?" and they topped of by saying "If you're up to it, yeah, of course!". I still remember their smile whilst saying this. Sounds cheesy I know, but it made a huge difference for me. Creating empathy is not an easy thing to do, you have to actually care, and believe me, they do.

Right there I thought "Hell, had this happened any other place I've worked in and I'd be flying through the door in no time! I mean, if I'm getting fucked like this on my second day on the job, there's no telling what's coming my way!" but given their approach and willingness to keep me in the team knowingly of my handicaps, the situation turned into one hell of an opportunity! I guess I'll have to postpone my wish to deeply learn C#, but the amount of stuff I'm going to learn is tremendous! I'd be a fool not to take it!

I also need to add that this was the first company where I really worked my ass off to get a position. The company has a strong take on culture and I did my best to know as much as I could about it. Personally, I identified myself with a lot of their values, and they had such a different management strategy (or lack of it) that made me feel like I'd go back to college, in the sense that I'd be learning every single day. (That's still true to this day)

### Hop on the train!

Things were moving at a rather fast pace on that project. We were shifting from a monolith application written in old .NET and DCOM (this is Microsoft's C++ superset before .NET came along. Yup, it's even worse than you think). Although the .NET part wasn't that great (had almost no tests and a lot of weird logic), nothing really compared to the intricacy of the DCOM code. Damn, it was nasty! Clearly there were a lot of problems with the application, but something was  clear as day, it was the result of many years of development (since 2000's) from a multitude of different people (hence the 5/6 layer cake). But hey, at the end of the day, it worked! There was no CD/CI, almost no tests, very dirty code, robust dependencies, the whole nine yards, but it actually worked!

Because the application didn't scale and was a pain in the ass to maintain, we started moving all this functionality to a micro-service architecture in the cloud. I mean, how great is that? Being able to just maintain the old code and focus on rebuilding the whole application with reasonable choice of tooling and framework? Holy shit that's awesome!

In just about 6/7 months we were leaving from AWS with Chef to Google Cloud with Kubernetes. Atop the whole micro-service thing going on, moving to a different cloud provider with a whole different orchestration? That's just crazy! Things were moving blazing fast and because of this, every developer was deeply encouraged to take in DevOps as part of their job. In order to trouble the Ops person you would first need to make sure you could run the whole thing in your own computer, which makes perfect sense, but there was no expert in GCloud/Docker/Kubernetes in the room, so it was a painstakingly and frustrating activity at times.

10/11 months after I started, I found myself doing Backend web applications (which I hadn't done since college) in two different clouds, with different orchestrations on a language (Java) and framework (SpringBoot) that I had zero (or close to it) experience. Shit, what a trip!

I have to say, it was the best tech stack I've worked on until this day! You can do things you never thought possible!

### "Who you gonna call?"

At the time of the cloud switch, I got invited to be a part of the on-call team. I'd never been in one, and besides the extra pay, I was excited to learn how the whole thing worked. If you've never been on-call, it is a very interesting experience (regardless of what I'm gonna say about it next, I really think everyone on the team should, at some point, **be a part of the on-call team**). You have to manage obvious things like time to fix and how to fix it, but a whole lot more as well. You're in a remote call mitigating the issue. You may find a fix, or you may have to revert back to the old release. In both you need to assess the impact to the customer. _Is there downtime? What features will be lost? Can this break something else? If it fails, what's the action plan to solve that?_ (If you want to know more about on-call, check out [Increment](https://increment.com/on-call/), they have great articles about it.)

The client (which has an almost US-only customer base) was 8 hours behind our timezone, which meant most incidents we had were after work hours and on the weekends (especially the weekends). As you can imagine, this inadvertently affects your personal life. Each of us were on one-week periods with 1/2 weeks per month. That means having your cellphone, laptop and mobile router with you at all times. It was expected you would check in around 1-10 minutes after you received the call. I remember several dinners and night outs with friends that got partially or totally interrupted by an incident. The saturdays I spent away assessing problems or fixing bugs, and sometimes not actually doing anything because that was the best option, were way more than I was counting on.

I remember at one point to loathe hearing my cellphone ring, even if it wasn't a pager call. One given week we had 7 incidents! Every time I'd hear the pager I'd think "Fuck! Not again...".

### The coda

My approach to my work was pretty much the same I had when I joined the company, and accepted the challenge of working completely outside my comfort zone. I was there to learn and make the best out of it. Because of this I really threw myself in, and the on-call was clearly a part of that. I had both slack and email on my cellphone and I'd reply to emails and messages off work hours, more often than not. I convinced myself I needed to do it because of the timezone difference, otherwise problems would take much longer to get solved.

At one point I found myself constantly checking slack or email whenever I was out with my friends, even when I was not on-call, and when I was, it was just worse.

Because of this I started to slack off, or completely stop, other personal stuff I was doing. I was missing rehearsals of the band I'm in, spending less quality time with my partner, never finishing setting up this blog, spending less and less time with my friends. **I was consumed by work! I convinced myself I had to be.** I constantly worried, on account of being the most junior on the team (micro-service/web app/cloud development), that I was always behind, that I had to push more and more! I sensed decisions were being made in which I wanted to be a part of, but I didn't feel knowledgeable enough to be included in them. **I felt left out.**

Working on a company with a very small amount of bureaucracy you'd think communication just flows, but you'd be wrong, it's very much the other way around. There is a lot of it but with tons of noise. And there is also a lot of information that you'll never know of. Not because people willingly hold it back, but because the attitude is so laid back, you make a lot of assumptions, one being that information (important one that is) just gets to people. I've discovered and keep discovering that's very untrue. In fact, I'm really struggling to make sure something very important reaches everyone, even by using the company's official communication channels. But that's a whole different story.

For about 14/15 months since I joined the project, a lot of Backend people had come in, and lot had left as well. Many of them were the people who started around the same time as myself. We talked, extensively, about their reasons for leaving. It really saddened me seeing those people go, although I completely understood what made them do so. After all I ended up doing the same for very similar reasons.

I remember telling them to try and get people together to solve their problems, because here we could do that! I mean, it's one of the foundations of the company! **C'mon! We can do it!!**

Through very different experiences I concluded that, at the end of the day, we're all humans, and we'll always be. No matter the liberty and responsibility you give to people, the human emotions and willpower will always kick in. People will dismiss wherever, whenever, they will push off, they will not care. Not in a maleficent way, but in a very natural human way.

A teacher of mine once said "You can only delude yourself if you elude yourself in the first place". And someone else spoke "Assumption is the mother of all fuck ups".

Although I don't totally agree with both statements, I did assume way to much and I did elude myself. I assumed every person would be fully responsible and respectful towards everyone in every situation, that **people would always care**! I mean, how could they not? We have the power to change so much, why wouldn't we take the opportunity? I craved it so much everywhere else I've worked!

This is our second home, this is what we make of it! It depends solely on us to make a change, to make it better everyday!

#### Why the fuck people just don't care??

I was pissed, I was tired, stressed and feeling like shit. I wanted to go on permanent vacation, but I also knew that if I did that, I would never want to come back. However, it would be extremely unfair and I'd be a coward. Why the hell should everything live up to my super high expectation? My shiny utopia?

### The fallout

I then realized the importance of expectation management. I personally feel that sharing something bad about a company is as important and sharing what's good. Especially for recruitment purposes. The respect I will have for the company goes miles up! We're not fooling anyone when we think everything is working just fine and we have no problems whatsoever. That's just bullshit and I'd steer away from the place.

Because I really wanted to come aboard, I easily dismissed the bad bits. Some I convinced myself I could live with and others I just didn't ask for, and that was a bad choice.

I decided I needed to change projects, I had to! Something significant to break the endless loop of self-deprecation, self-loathing hurtful mindset. Around two months ago I made the change. This was my chance to break my downward spiral, to get back what I'd lost, to feel good again about what I do. Going in I knew I had to manage how much time I'd be working and more importantly, what off-work activities I'd be putting myself into.

I started taking cooking a little more seriously. I've always enjoyed cooking, especially for others, and this was a chance to pick it up. I finally put myself to set up this blog, about two years after I thought of doing it. I started to organize meetings within the company, and get people together to discuss very important things, like what we think is not working and we could do better! Or being part of driving a salary transparency group to help people break the salary taboo! To show people **ignorance is not a blessing**, **transparency and communication are a must** so trust can exist! I've set myself to take the reins on what people talked to me about, but didn't have the will or the patience to get them going. And the most awesome part of this is, all the content and the decisions are made by a group of people, not a single person. I'm not leading anything, I'm just there as everybody else, participating as an individual within a group. My voice isn't higher or lower than anybody elses, it's the same! The only difference is, I'm always looking out for the wagon, so that when it stops, I'm there to give it a push.

Dealing with people is hard, it's very irrational and very frustrating. I understand it now. I understand that, although people have the desire to do something, when they don't do it, it doesn't mean it's necessarily the best thing. Why aren't they doing it? Are they afraid of the consequences? Are they doubtful of whom to speak? Are they shy or introverts?

When you start talking to very different people and start seeing a pattern, you quickly reach the conclusion there is in fact something common, and maybe that in itself is an opportunity to try and get people together. If you look at our history, a lot of important events took place when people got together, so why should this be any different? I don't think it should. Sometimes we just have to say "Fuck it!" and do it. If it all goes to shit, screw it! At least we tried, and most important of all, we got something out of it!

Recently an ex-colleague of mine joined the company. He was clearly as excited as I was back when I started, after all it was his second try after a long set of months. One of the first things I told him was "Manage your expectations" to which he replied "You're like the second or third person telling me that! Why?" and then I briefly explained what I said above.

Looking back, I still think I had the right spirit, nevertheless I was very naive. People will always be people. There is so much going on in human relations that's very difficult to predict or understand something. Working on a very predictable and precise field such as software development, it is an interesting contrast to reconcile both ends.

Today, I don't have the client's slack or the company's email on my cellphone, I purposely separate work from personal stuff and I leave work at the workplace. This doesn't mean I'm not there if something is on fire, because I will be, but it will only happen in real emergencies. I constantly ask myself, "If I were to do all this work at home, what would it achieve? There's always more work! If the app fails, what's the worst can happen?" (I'm not in a critical project like health or insurance, so the answer is, someone will get slightly annoyed and come back later). You need to manage this, because if you're giving your all to work, what gets left to your closest friends and family? Work will always be there, it always is! If you're not well it will affect your everything and you're actually achieving the opposite of what you set out to do.

Stop for a moment, focus on what's really important and cruise control on everything else. It will be better for you and for everyone around you.

I usually say "Life is like a VHS tape. If you seek backwards and play it, you'll see the exact same thing." (except when I was a kid and re-watched The Lion King. I strongly believed Mufasa wouldn't die that one time... and then I cried).

Life is made of decisions, and the best you can do is make sure you're making the right one at any given time. The rest just plays itself out. If I could have done anything differently, I wouldn't, because I made the best decisions I could have made.

---
As a dear friend of mine said after reading this post "Hakuna Matata".

If you want to read a very good post about the same topic that dives more deeply into what causes a burnout, and how can you know it before it's too late, check out [Kieran Tie's blog](https://kierantie.com/a/burnout/). It served me as an inspiration to write this one.