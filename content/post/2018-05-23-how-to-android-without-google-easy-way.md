---
author: "Pablo Portugués"
date: 2018-05-23
linktitle: How to Android without Google [easy way]
menu:
  main:
    parent: general
next: 
prev: 
title: How to Android without Google [easy way]
description: A comprehensive guide on how to setup your android phone without anything Google related [the easy way]
weight: 10
keywords: [
  "android",
  "google",
  "lineageos",
  "microg",
  "yalpstore",
  "f-droid",
  "xposed",
  "de-googlify"
]
tags: [
    "general",
    "android"
]
---

{{< figure src="/post-content/2018-05-23/its-not-me-its-you-easy-way.jpg" class="post-image" >}}

This guide shows how to install LineageOS without GApps with the help of **signature spoofing** and **microG**, so that you can have **Push Notifications, Location Services and the like**, without needing to have Google Play Services installed. (Without Google-anything for that matter)

It was made possible by the hard work of creators, maintainers and community around [LineageOS](https://lineageos.org/), [microG](https://microg.org/), [XPosedFramework](http://repo.xposed.info/), [F-Droid](https://f-droid.org/), [Yalp Store](https://github.com/yeriomin/YalpStore) and many others.

1. **NOTE 1:** every `adb` and `fastboot` commands I describe here assume the path you're in is the path with the specified files. If it's not, either move there or point to it from where you are.
1. **NOTE 2:** During the whole process I'm assuming the device is connected to your computer.
1. **NOTE 3:** This tutorial has references to the OnePlus One device, but it should apply to any other phone that's supported by LineageOS. Where you see `bacon` (OPO codename) that means you should search that content's link for your own phone. You can find the codename for your phone here: https://wiki.lineageos.org/devices/

**DISCLAIMER:** Following the steps here might brick your phone or cause other types of problems. Follow it at your own risk and make sure you understand what you're doing.

There are at least two ways of achieving this - the *straightforward/easy way* (this guide) or the [*step-by-step/hard way*]({{<ref "2018-05-23-how-to-android-without-google-hard-way.md">}}). Choose the one it best fits your needs.

---

### Setting up the playground

1. Backup your phone (apps, settings, files, images, etc)
1. Install `adb` and `fastboot` on your device - follow instructions [here](https://wiki.lineageos.org/adb_fastboot_guide.html)

#### A little context

This approach consists in flashing a custom version of LineageOS with all of microG components. The reason why migroG isn't a part of LineageOS itself, is because it's maintainers have rejected the signature spoofing capability to be part of Lineage, arguing security problems, despite several attempts from the microG team to prove them it wouldn't bring any extra risk to it (more on that [here](https://lineage.microg.org/#faq6)).

So what microG team did was patch in their components on top of Lineage and create new releases, so any person that would want Lineage w/ microG to avoid anything Google, could have a solution almost out of the box (the step-by-step way shows why the process can become a bit complicated). They release their version in a very quick fashion and to [all devices supported by Lineage](https://download.lineage.microg.org/).

I personally would go for this guide, because you can set everything up without having to root your phone, which is not the case for step-by-step way.

**So let's get to it then.**

#### Installation

##### Download the following files:

1. [Your LineageOS w/ microG ROM](https://download.lineage.microg.org/)
1. [The TWRP recovery](https://dl.twrp.me/bacon/)

##### Unlock the bootloader - instructions [here](https://wiki.lineageos.org/devices/bacon/install)

1. Boot into the bootloader - `adb reboot bootloader`
1. [Install the customer recovery with fastboot](https://wiki.lineageos.org/devices/bacon/install) - `fastboot flash recovery <path-to>/twrp-x.x.x-x-bacon.img`
1. Boot into recovery (volume down + power button - varies by phone)
1. Go to: `Wipe → Factory Reset` - this will be enough for the majority of cases. **Advanced Wipe** and **Format Data** will give you complementary choices if needed - like if you're changing ROMs (CM to LineageOS) and have signing problems. In that case there may a possibility you have to delete everything. If you do that, you may need to `Mount` otherwise the file system won't work

##### Installing the ROM (while in recovery)

1. `adb push <path-to>/lineage-14.1-20180516-nightly-bacon-signed.zip /sdcard/` - no need to copy this one if you're planning on installing it via `adb sideload` (_Advanced → ADB Sideload_ then `adb sideload <path-to>/lineage-14.1-20180516-nightly-bacon-signed.zip`)
1. Go to Install, select the file of your custom ROM and install
1. Reboot

##### After everything loads up and you've setup the device, head over to the microG app.

1. Start by allowing access to what it asks (it really is needed)
1. Enable _Google device registration_
1. Enable _Google Cloud Messaging_
1. Turn battery optimization **off** for microG, otherwise it won't work as the system will likely shut it down. (Google Play Services aren't battery optimized as well, at least here you have a choice)
1. Go to _Settings → Battery → More Options → Battery Optimization → All Apps → microG Services Core → Don't use optimization_


Head on back to microG and open the _self-check_. All boxes should now be ticked. Some boxes in UnifiedIP section may not be ticked, however it doesn't mean location services aren't working. For more info check the _Post Install_ section [here](https://lineage.microg.org/#faq).

Lastly we just need to check if our location services and push notifications are working.


### Testing location services and push notifications

Now that we have everything setup, time to actually test if it's working as expected.

#### Location Services

This one is easy. First enable the device's location by going into _Settings → Location → Mode_ and set to _High accuracy_ (notice that it won't work with _Device only_).

Head on over to the Browser app and open OpenStreetMaps (or whatever maps/location service you want), allow it to access the devices location, and click to show your location.

{{< figure src="/post-content/2018-05-23/test-locations.png" class="post-image-single-vertical" >}}

#### Push notifications

We're just missing Push Notifications so let's test that. Easiest way is to use an app - [Push Notification Tester](https://play.google.com/store/apps/details?id=com.firstrowria.pushnotificationtester).

To install apps from Play Store without Play Store app, check [this](#where-to-get-apps-that-are-not-on-f-droid). Proceed to install _Push Notification Tester_ through _Yalp Store_.

Running it should get you all green tick boxes and a push notification in the end.

{{< figure src="/post-content/2018-05-23/push-notification-test.png" class="post-image" >}}


And that's it, you're officially setup with a fully functional device **running Android** with **no Google** whatsoever!

### Where to get apps that are not on F-Droid

Thanks to a project called [Yalp Store](https://github.com/yeriomin/YalpStore) you can download apps and keep them updated from the Play Store. I've used mirrors in the past, but this is by far the safest solution for me, as well as the easiest to use. You can download the [app on F-Droid](https://f-droid.org/en/packages/com.github.yeriomin.yalpstore/) and check how it works on [their page](https://github.com/yeriomin/YalpStore).

---

### Thoughts and motivations

The main reason why I went forward and did this was to actually get rid of all that's Google on my phone. This doesn't mean that I won't install some GApp, because I have, in the past, but because I have full control over it as I have with any other application. What bothered the most is exactly that, Android is free and open-source, but somehow it doesn't seem that straightforward to have what you need, without something Google in there.

The funny thing is that there isn't really a way not to deal with Google. If you read the purpose of microG you'll understand it mainly serves as a proxy, something that still communicates with Google servers. The fact is, there is no way around that. The whole Android ecosystem is dependent of Google, either because of Google Play services or Play Store (every other store is officially unrecommended - they know what it's best for you - _Apple dejá vú much?_). The former means that every app on Play Store registers push notifications through Google servers, so if you don't have something that connects to those servers, you're pretty much screwed and won't get push notifications (K-9 mail does this differently though). Albeit Android being free and open-source, each day it seems less and less so. Just look at this tutorial on how to run an Android version without Google.

If you think about it, you have an open platform with an ecosystem around that is tightly dependent on a single company. Personally, that is not the purpose of Android, and in the long run, we're just losing because of that. Also, if you want to develop an app you need to comply with Google Play Store policy, whether you like it or not, and if you make yearnings out of it, 30% of that goes to Google.

Take a moment in the memory lane to the time Microsoft ruled the web with Internet Explorer. If you could gather all the hate that browser has, you could probably build a couple of atomic bombs. Fast forward to now and Chrome rules the web, and if your attentive enough, you'll notice the messages like "Works best on Google Chrome" or "Sorry, only works with Google Chrome, download it here" are getting more and more frequent. The concept of open web is getting way different that what it used to be.

And don't get me wrong, I've absolutely no problem with Play Store or Play Services, I just think there needs to be space for an alternative that doesn't entirely depend on Google and doesn't involve you becoming a tech ninja.

---
This is one of the cases that just because you can, it means you should!